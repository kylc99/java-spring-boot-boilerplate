package com.mewebstudio.javaspringbootboilerplate.config;

import jakarta.servlet.RequestDispatcher;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

@Configuration
public class CustomErrorAttributes extends DefaultErrorAttributes {

    public static final String KEY_MESSAGE = "message";
    public static final String KEY_ITEMS = "items";

    @Override
    public final Map<String, Object> getErrorAttributes(final WebRequest webRequest,
        final ErrorAttributeOptions options) {
        Map<String, Object> errorAttributes = super.getErrorAttributes(webRequest, options);
        Object errorMessage = webRequest.getAttribute(RequestDispatcher.ERROR_MESSAGE, RequestAttributes.SCOPE_REQUEST);

        Map<String, Object> map = new HashMap<>();
        map.put("status", errorAttributes.get("status"));
        if (errorMessage != null) {
            String message = (String) (Objects.nonNull(errorAttributes.get(KEY_MESSAGE)) ? errorAttributes.get(KEY_MESSAGE)
                : "Server error");
            map.put(KEY_MESSAGE, message);
        }
        if (Objects.nonNull(errorAttributes.get(KEY_ITEMS))) {
            map.put(KEY_ITEMS, errorAttributes.get(KEY_ITEMS));
        }

        return map;
    }
}
