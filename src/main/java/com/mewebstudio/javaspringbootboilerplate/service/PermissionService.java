package com.mewebstudio.javaspringbootboilerplate.service;

import com.mewebstudio.javaspringbootboilerplate.entity.Permission;
import com.mewebstudio.javaspringbootboilerplate.repository.PermissionRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class PermissionService {

    final PermissionRepository permissionRepository;

    public List<Permission> findAll() {
        return permissionRepository.findAll();
    }

    public void saveAll(List<Permission> permissions) {
        permissionRepository.saveAll(permissions);
    }

    public boolean existByFunction(String function) {
        return permissionRepository.existsByFunction(function);
    }
}
