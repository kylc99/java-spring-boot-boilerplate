package com.mewebstudio.javaspringbootboilerplate.dto.annotation;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Target({METHOD, TYPE, FIELD})
@Retention(RUNTIME)
public @interface DynamicPermission {

    String description();
}
