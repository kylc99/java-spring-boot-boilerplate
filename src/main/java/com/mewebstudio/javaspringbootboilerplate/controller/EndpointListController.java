package com.mewebstudio.javaspringbootboilerplate.controller;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.util.pattern.PathPattern;

@RestController
@RequestMapping("/endpoints")
@RequiredArgsConstructor
public class EndpointListController {

    private final RequestMappingHandlerMapping requestMappingHandlerMapping;

    @GetMapping
    public Set<EndpointInfo> listAllEndpoints() {
        Set<EndpointInfo> urlMap = new HashSet<>();

        Map<RequestMappingInfo, HandlerMethod> map = requestMappingHandlerMapping.getHandlerMethods();

        map.forEach((key, value) -> {
            if (key.getPathPatternsCondition() != null) {
                Set<PathPattern> patterns = key.getPathPatternsCondition().getPatterns();
                var methods = key.getMethodsCondition().getMethods().toString();
                patterns.forEach(
                    url -> urlMap.add(new EndpointInfo(url.getPatternString(), methods, value.getMethod().getName())));
            }
        });

        return urlMap;
    }

    @Data
    @AllArgsConstructor
    public static class EndpointInfo {

        private String urlPattern;
        private String httpMethod;
        private String classMethod;
    }
}
