package com.mewebstudio.javaspringbootboilerplate.aop;

import com.mewebstudio.javaspringbootboilerplate.entity.Permission;
import com.mewebstudio.javaspringbootboilerplate.service.AuthenticationService;
import com.mewebstudio.javaspringbootboilerplate.service.MessageSourceService;
import com.mewebstudio.javaspringbootboilerplate.service.PermissionService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;

@Aspect
@Slf4j
@Component
@RequiredArgsConstructor
public class DynamicPermissionAspect {

    final PermissionService permissionService;
    final AuthenticationService authenticationService;
    final MessageSourceService messageSourceService;

    @Pointcut("@within(org.springframework.web.bind.annotation.RestController)")
    public void restController() {
    }

    @Pointcut("@within(org.springframework.stereotype.Controller)")
    public void controller() {
    }


    @Before("restController() || controller()")
    public void myAdviceRestController(JoinPoint joinPoint) {
        String functionName = joinPoint.getSignature().getDeclaringType().getName() + "." + joinPoint.getSignature().getName();
        log.info("functionName: {}", functionName);
        List<Permission> permissions = permissionService.findAll();
        boolean isCheckPermission = permissions.stream().anyMatch(item -> item.getFunction().equals(functionName));
        if (isCheckPermission && !authenticationService.isGranted(functionName)) {
            throw new AccessDeniedException(messageSourceService.get("access_denied"));
        }
    }
}
