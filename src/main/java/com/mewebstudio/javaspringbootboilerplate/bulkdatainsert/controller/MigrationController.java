package com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.controller;

import com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.entity.Product;
import com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.entity.ProductImportHistory;
import com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.repository.ProductImportHistoryRepository;
import com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.service.MigrationService;
import com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.service.ProductService;
import com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.utils.ProductDataBuilder;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/migrations")
public class MigrationController {


    final ProductImportHistoryRepository productImportHistoryRepository;

    final ProductService productService;
    final MigrationService migrationService;

    @PostMapping("/products")
    public ProductImportHistory saveAllProduct(@RequestParam Long total) {
        LocalDateTime startInsert = LocalDateTime.now();
        ProductImportHistory importHistory = ProductImportHistory
            .builder()
            .startInsert(startInsert)
            .build();
        productImportHistoryRepository.save(importHistory);
        List<Product> productData = ProductDataBuilder
            .builder()
            .setTotal(total.intValue())
            .setImportHistory(importHistory)
            .build();

        log.info("Using id identity, ");
        migrationService.saveAllJdbcBatchCallable(productData,
            "INSERT INTO %s (title, stock, price, import_history_id) VALUES (?, ?, ?, ?)");
        LocalDateTime endInsert = LocalDateTime.now();
        Long duration = ChronoUnit.MILLIS.between(startInsert, endInsert);
        importHistory.setEndInsert(endInsert);
        importHistory.setTotalRecords(total);
        importHistory.setDuration(duration);
        productImportHistoryRepository.save(importHistory);
        log.info("{} data inserted, duration in second {}", productData.size(), duration);
        return importHistory;
    }
}
