package com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.controller;

import com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.entity.Product;
import com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.entity.ProductImportHistory;
import com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.repository.ProductImportHistoryRepository;
import com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.service.ProductService;
import com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.utils.ProductDataBuilder;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductController {


    final ProductImportHistoryRepository productImportHistoryRepository;

    final ProductService productService;

    @PostMapping
    public void saveAllProduct(@RequestParam Integer total) {
        LocalDateTime startInsert = LocalDateTime.now();
        ProductImportHistory importHistory = ProductImportHistory
            .builder()
            .startInsert(startInsert)
            .build();
        productImportHistoryRepository.save(importHistory);
        List<Product> productData = ProductDataBuilder
            .builder()
            .setTotal(total)
            .setImportHistory(importHistory)
            .build();

        log.info("Using id identity, ");
        productService.saveAllJdbcBatchCallable(productData);
        LocalDateTime endInsert = LocalDateTime.now();
        importHistory.setEndInsert(endInsert);
        productImportHistoryRepository.save(importHistory);
        log.info(productData.size() + " data inserted, duration in second " + ChronoUnit.MILLIS.between(startInsert, endInsert));
    }

    @GetMapping
    public List<Product> getAllProduct() {
        return productService.getAll();
    }
}
