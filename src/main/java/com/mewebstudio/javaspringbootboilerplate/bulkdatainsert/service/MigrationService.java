package com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.service;

import com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.entity.Product;
import com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.utils.ListUtil;
import com.zaxxer.hikari.HikariDataSource;
import jakarta.persistence.Table;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MigrationService {

    private static final ConcurrentMap<String, BiConsumer<PreparedStatement, ?>> ACTIONS = new ConcurrentHashMap<>();
    private final HikariDataSource hikariDataSource;
    @Value("${spring.jpa.properties.hibernate.jdbc.batch_size:1000}")
    private int batchSize;

    static {
        registerPreparedStatements();
    }

    public <Q> void saveAllJdbcBatchCallable(List<Q> productData, String sqlInsert) {
        log.info("insert using jdbc batch, threading");
        log.info("cp size {}", hikariDataSource.getMaximumPoolSize());
        log.info("batch size {}", batchSize);
        List<List<Q>> listOfBookSub = ListUtil.createSubList(productData, batchSize);
        ExecutorService executorService = Executors.newFixedThreadPool(hikariDataSource.getMaximumPoolSize());
        List<Callable<Integer>> callables = listOfBookSub.stream().map(sublist ->
            (Callable<Integer>) () -> {
                log.info("Inserting {} using callable from thread {}", sublist.size(), Thread.currentThread().getName());
                saveAllJdbcBatch(sublist, sqlInsert);
                return sublist.size();
            }).toList();
        try {
            List<Future<Integer>> futures = executorService.invokeAll(callables);
            int count = futures.stream().mapToInt(this::getFutureResult).sum();
            log.info("Inserting count: {}", count);
        } catch (InterruptedException e) {
            log.error("Thread interrupted: {}", e.getMessage(), e);
            Thread.currentThread().interrupt();
        } finally {
            executorService.shutdown();
        }
    }

    private <Q> void saveAllJdbcBatch(List<Q> qList, String sqlInsert) {
        String tableName = getTableName(qList.get(0));
        String sql = String.format(sqlInsert, tableName);
        try (Connection connection = hikariDataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(sql)) {
            int counter = 0;
            for (Q q : qList) {
                statement.clearParameters();
                prepareStatement(q, statement);
                statement.addBatch();
                if (++counter % batchSize == 0 || counter == qList.size()) {
                    statement.executeBatch();
                    statement.clearBatch();
                }
            }
        } catch (Exception e) {
            log.error("Error saving data", e);
        }
    }

    private <Q> void prepareStatement(Q q, PreparedStatement statement) {
        String objClass = q.getClass().getName();
        @SuppressWarnings("unchecked")
        var action = (BiConsumer<PreparedStatement, Q>) ACTIONS.get(objClass);
        if (action != null) {
            try {
                action.accept(statement, q);
            } catch (Exception e) {
                log.error("Error preparing statement", e);
            }
        } else {
            log.info("No action defined for type: {}", objClass);
        }
    }

    @SuppressWarnings("unchecked")
    private static void preparedStatementProduct(PreparedStatement statement, Product product) {
        try {
            AtomicInteger counter = new AtomicInteger(1);
            statement.setObject(counter.getAndIncrement(), product.getTitle());
            statement.setObject(counter.getAndIncrement(), product.getStock());
            statement.setObject(counter.getAndIncrement(), product.getPrice());
            statement.setObject(counter.getAndIncrement(), product.getImportHistory().getId());
        } catch (SQLException e) {
            log.error("Error setting preparedStatementProduct ", e);
        }
    }

    private int getFutureResult(Future<Integer> future) {
        try {
            return future.get();
        } catch (ExecutionException | InterruptedException e) {
            log.error("Error getting future result", e);
            Thread.currentThread().interrupt();
        }
        return 0;
    }

    private <Q> String getTableName(Q obj) {
        Table tableAnnotation = obj.getClass().getAnnotation(Table.class);
        if (tableAnnotation != null) {
            return tableAnnotation.name();
        }
        throw new IllegalArgumentException("No @Table annotation found on class: " + obj.getClass().getName());
    }

    private static void registerPreparedStatements() {
        Method[] methods = MigrationService.class.getDeclaredMethods();
        for (Method method : methods) {
            if (method.getParameterCount() == 2 && method.getParameterTypes()[0] == PreparedStatement.class) {
                String paramType = method.getParameterTypes()[1].getName();
                ACTIONS.put(paramType, (statement, param) -> {
                    try {
                        method.invoke(null, statement, param);
                    } catch (Exception e) {
                        log.error("Error invoking method", e);
                    }
                });
            }
        }
    }
}
