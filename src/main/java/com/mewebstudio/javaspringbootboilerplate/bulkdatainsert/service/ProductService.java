package com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.service;

import com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.entity.Product;
import com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.repository.ProductRepository;
import com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.utils.ListUtil;
import com.zaxxer.hikari.HikariDataSource;
import jakarta.persistence.Table;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductService {

    final HikariDataSource hikariDataSource;

    final ProductRepository productRepository;

    @Value("${spring.jpa.properties.hibernate.jdbc.batch_size}")
    private int batchSize;

    public void saveAll(List<Product> productData) {
        log.info("insert using saveAll, hibernate batch");
        productRepository.saveAll(productData);
    }

    public void saveAllHibernateBatch(List<Product> productData) {
        log.info("insert using saveAll, hibernate + manual batch");
        List<Product> productDataTemp = new ArrayList<>();
        int counter = 0;
        for (Product product : productData) {
            productDataTemp.add(product);
            if ((counter + 1) % batchSize == 0 || (counter + 1) == productData.size()) {
                productRepository.saveAll(productDataTemp);
                productDataTemp.clear();
            }
            counter++;
        }
    }

    public void saveAllJdbcBatch(List<Product> productData) {
        String sql = String.format("INSERT INTO %s (title, stock, price, import_history_id) VALUES (?, ?, ?, ?)",
            Product.class.getAnnotation(Table.class).name());
        try (Connection connection = hikariDataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            int counter = 0;
            for (Product product : productData) {
                AtomicInteger index = new AtomicInteger(0);
                statement.clearParameters();
                statement.setString(index.incrementAndGet(), product.getTitle());
                statement.setInt(index.incrementAndGet(), product.getStock());
                statement.setFloat(index.incrementAndGet(), product.getPrice());
                statement.setInt(index.incrementAndGet(), product.getImportHistory().getId());
                statement.addBatch();
                if ((counter + 1) % batchSize == 0 || (counter + 1) == productData.size()) {
                    statement.executeBatch();
                    statement.clearBatch();
                }
                counter++;
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public void saveAllJdbcBatchCallable(List<Product> productData) {
        log.info("insert using jdbc batch, threading");
        log.info("cp size {}", hikariDataSource.getMaximumPoolSize());
        log.info("batch size {}", batchSize);
        List<List<Product>> listOfBookSub = ListUtil.createSubList(productData, batchSize);
        ExecutorService executorService = Executors.newFixedThreadPool(hikariDataSource.getMaximumPoolSize());
        List<Callable<Integer>> callables = listOfBookSub.stream().map(sublist ->
            (Callable<Integer>) () -> {
                log.info("Inserting {} using callable from thread {}", sublist.size(), Thread.currentThread().getName());
                saveAllJdbcBatch(sublist);
                return sublist.size();
            }).toList();
        try {
            List<Future<Integer>> futures = executorService.invokeAll(callables);
            int count = 0;
            for (Future<Integer> future : futures) {
                count += future.get();
            }
            log.info("Inserting count: {}", count);
        } catch (InterruptedException | ExecutionException e) {
            log.error(e.getMessage(), e);
        }
    }

    public List<Product> getAll() {
        return productRepository.getAllByTime();
    }
}
