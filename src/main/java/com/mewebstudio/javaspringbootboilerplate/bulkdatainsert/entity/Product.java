package com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.entity;

import jakarta.persistence.*;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@Entity
@Table(name = "products")
@NoArgsConstructor
@AllArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private Integer stock;

    private Float price;

    private LocalDateTime createAt;

    @ManyToOne
    @JoinColumn(name = "import_history_id")
    private ProductImportHistory importHistory;
}
