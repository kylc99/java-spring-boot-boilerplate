package com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.entity;

import jakarta.persistence.*;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@Entity
@Table(name = "product_import_history")
@NoArgsConstructor
@AllArgsConstructor
public class ProductImportHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private LocalDateTime startInsert;
    private LocalDateTime endInsert;
    private Long totalRecords;
    private Long duration;
}