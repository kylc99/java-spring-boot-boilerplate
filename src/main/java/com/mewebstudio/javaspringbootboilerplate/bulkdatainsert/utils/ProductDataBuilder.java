package com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.utils;

import com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.entity.Product;
import com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.entity.ProductImportHistory;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class ProductDataBuilder {

    private static final float HALF = 0.5f;
    private final Random random = new Random();
    private ProductImportHistory importHistory;
    private int total;

    public static ProductDataBuilder builder() {
        return new ProductDataBuilder();
    }

    public ProductDataBuilder setImportHistory(ProductImportHistory history) {
        importHistory = history;
        return this;
    }

    public ProductDataBuilder setTotal(int total) {
        this.total = total;
        return this;
    }

    public List<Product> build() {
        return IntStream.range(0, total)
            .mapToObj(val -> Product.builder()
                .title("Product " + val)
                .stock(random.nextInt(total))
                .price(random.nextInt(total) * HALF)
                .importHistory(importHistory)
                .build()
            ).toList();
    }
}
