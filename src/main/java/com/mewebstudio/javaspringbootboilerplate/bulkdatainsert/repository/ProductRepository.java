package com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.repository;

import com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.entity.Product;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ProductRepository extends JpaRepository<Product, Integer> {

    @Query(nativeQuery = true, value = "select * testprocedure1();")
    List<Product> getAllByTime();
}