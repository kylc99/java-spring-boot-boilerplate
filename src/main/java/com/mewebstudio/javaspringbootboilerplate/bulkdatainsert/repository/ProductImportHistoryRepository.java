package com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.repository;

import com.mewebstudio.javaspringbootboilerplate.bulkdatainsert.entity.ProductImportHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductImportHistoryRepository extends JpaRepository<ProductImportHistory, Integer> {

}
