package com.mewebstudio.javaspringbootboilerplate.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "permissions", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"function"}, name = "uk_permissions_function")
}, indexes = {
    @Index(columnList = "description", name = "idx_permissions_description")
})
public class Permission extends AbstractBaseEntity {

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "function", nullable = false)
    private String function;

    @Column(name = "endpoint", nullable = false)
    private String endpoint;

    @Column(name = "method_type", nullable = false, length = 20)
    private String methodType;
}
