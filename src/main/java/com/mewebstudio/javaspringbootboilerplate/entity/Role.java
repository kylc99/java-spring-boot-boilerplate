package com.mewebstudio.javaspringbootboilerplate.entity;

import com.mewebstudio.javaspringbootboilerplate.util.Constants;
import jakarta.persistence.*;
import java.util.HashSet;
import java.util.Set;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.NaturalId;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "roles", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"name"}, name = "uk_roles_name")
}, indexes = {
    @Index(columnList = "name", name = "idx_roles_name")
})
public class Role extends AbstractBaseEntity {

    @Enumerated(EnumType.STRING)
    @Column(name = "name", length = 16)
    @NaturalId
    private Constants.RoleEnum name;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    @JoinTable(name = "role_permissions",
        joinColumns = @JoinColumn(
            name = "role_id",
            foreignKey = @ForeignKey(
                name = "fk_role_permissions_role_id",
                foreignKeyDefinition = "FOREIGN KEY (role_id) REFERENCES roles (id) ON DELETE CASCADE"
            ),
            nullable = false
        ),
        inverseJoinColumns = @JoinColumn(
            name = "permission_id",
            foreignKey = @ForeignKey(
                name = "fk_role_permissions_permission_id",
                foreignKeyDefinition = "FOREIGN KEY (permission_id) REFERENCES permissions (id) ON DELETE CASCADE"
            ),
            nullable = false
        ),
        uniqueConstraints = {
            @UniqueConstraint(
                columnNames = {"role_id", "permission_id"},
                name = "uk_role_permissions_role_id_permission_id"
            )
        }
    )
    @Builder.Default
    private Set<Permission> permissions = new HashSet<>();

    public Role(final Constants.RoleEnum name) {
        this.name = name;
    }
}
