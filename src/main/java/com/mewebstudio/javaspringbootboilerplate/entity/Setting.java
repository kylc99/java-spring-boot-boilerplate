package com.mewebstudio.javaspringbootboilerplate.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "settings", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"key"}, name = "uk_settings_key")
}, indexes = {
    @Index(columnList = "value", name = "idx_settings_value")
})
@Entity
public class Setting extends AbstractBaseEntity {

    @Column(name = "key", nullable = false)
    private String key;

    @Column(name = "value", columnDefinition = "text")
    private String value;
}
