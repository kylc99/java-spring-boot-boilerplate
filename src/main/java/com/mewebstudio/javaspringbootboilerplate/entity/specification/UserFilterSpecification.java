package com.mewebstudio.javaspringbootboilerplate.entity.specification;

import com.mewebstudio.javaspringbootboilerplate.entity.*;
import com.mewebstudio.javaspringbootboilerplate.entity.specification.criteria.UserCriteria;
import jakarta.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.NonNull;

@RequiredArgsConstructor
public final class UserFilterSpecification implements Specification<User> {

    private final transient UserCriteria criteria;

    @Override
    public Predicate toPredicate(@NonNull final Root<User> root,
        @NonNull final CriteriaQuery<?> query,
        @NonNull final CriteriaBuilder builder) {
        if (criteria == null) {
            return null;
        }

        List<Predicate> predicates = new ArrayList<>();

        if (criteria.getRoles() != null && !criteria.getRoles().isEmpty()) {
            Join<User, Role> roleJoin = root.join(User_.ROLES);
            predicates.add(
                builder.in(roleJoin.get(Role_.NAME)).value(criteria.getRoles())
            );
        }

        if (criteria.getIsAvatar() != null) {
            if (Boolean.TRUE.equals(criteria.getIsAvatar())) {
                predicates.add(builder.isNotNull(root.get(User_.AVATAR)));
            } else {
                predicates.add(builder.isNull(root.get(User_.AVATAR)));
            }
        }

        if (criteria.getCreatedAtStart() != null) {
            predicates.add(
                builder.greaterThanOrEqualTo(root.get(AbstractBaseEntity_.CREATED_AT), criteria.getCreatedAtStart())
            );
        }

        if (criteria.getCreatedAtEnd() != null) {
            predicates.add(
                builder.lessThanOrEqualTo(root.get(AbstractBaseEntity_.CREATED_AT), criteria.getCreatedAtEnd())
            );
        }

        if (criteria.getIsBlocked() != null) {
            if (Boolean.TRUE.equals(criteria.getIsBlocked())) {
                predicates.add(builder.isNotNull(root.get(User_.BLOCKED_AT)));
            } else {
                predicates.add(builder.isNull(root.get(User_.BLOCKED_AT)));
            }
        }

        if (criteria.getQ() != null) {
            String q = String.format("%%%s%%", criteria.getQ().toLowerCase());

            predicates.add(
                builder.or(
                    builder.like(builder.lower(root.get(AbstractBaseEntity_.ID).as(String.class)), q),
                    builder.like(builder.lower(root.get(User_.EMAIL)), q),
                    builder.like(builder.lower(root.get(User_.NAME)), q),
                    builder.like(builder.lower(root.get(User_.LAST_NAME)), q)
                )
            );
        }

        if (!predicates.isEmpty()) {
            query.where(predicates.toArray(new Predicate[0]));
        }

        return query.distinct(true).getRestriction();
    }

    public static Specification<User> idLike(String id) {
        return (root, query, builder) -> builder.like(builder.lower(root.get(AbstractBaseEntity_.ID).as(String.class)), id);
    }

    public static Specification<User> byId(UUID id) {
        return (root, query, builder) -> builder.equal(root.get(AbstractBaseEntity_.ID), id);
    }

    public static Specification<User> nameLike(String name) {
        return (root, query, builder) -> builder.like(builder.lower(root.get(User_.NAME)), "%" + name + "%");
    }

    public static Specification<User> emailLike(String email) {
        return (root, query, builder) -> builder.like(builder.lower(root.get(User_.EMAIL)), email);
    }

    public static Specification<User> lastNameLike(String name) {
        return (root, query, builder) -> builder.like(builder.lower(root.get(User_.LAST_NAME)), name);
    }
}
