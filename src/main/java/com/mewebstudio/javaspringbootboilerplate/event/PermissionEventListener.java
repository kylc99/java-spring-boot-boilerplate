package com.mewebstudio.javaspringbootboilerplate.event;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mewebstudio.javaspringbootboilerplate.dto.annotation.DynamicPermission;
import com.mewebstudio.javaspringbootboilerplate.entity.Permission;
import com.mewebstudio.javaspringbootboilerplate.service.PermissionService;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;
import org.reflections.scanners.Scanners;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@Slf4j
@RequiredArgsConstructor
public class PermissionEventListener {

    final PermissionService permissionService;

    @EventListener
    public void handleContextRefresh(ContextRefreshedEvent event) throws JsonProcessingException {
        long start = System.currentTimeMillis();
        Reflections reflections = new Reflections(new ConfigurationBuilder()
            .setUrls(ClasspathHelper.forPackage("com.mewebstudio.javaspringbootboilerplate.controller"))
            .setScanners(Scanners.MethodsAnnotated, Scanners.TypesAnnotated, Scanners.FieldsAnnotated));

        Set<Method> methods = reflections.getMethodsAnnotatedWith(DynamicPermission.class);
        List<Permission> storePermissions = permissionService.findAll();
        List<Permission> permissions = new ArrayList<>();

        methods.forEach(m -> {
            String function = m.getDeclaringClass().getName() + "." + m.getName();
            boolean exist = storePermissions.stream().anyMatch(p -> function.equals(p.getFunction()));
            if (!exist) {
                String[] classEndpoints = m.getDeclaringClass().getAnnotation(RequestMapping.class).value();
                String classEndpoint = classEndpoints.length > 0 ? classEndpoints[0] : "";
                var permission = Permission.builder()
                    .description(m.getAnnotation(DynamicPermission.class).description())
                    .function(function)
                    .build();
                Annotation[] annotations = m.getAnnotations();
                for (Annotation an : annotations) {
                    if (isRequestMappingAnnotation(an)) {
                        processMapping(an, permission, classEndpoint);
                    }
                }
                permissions.add(permission);
            }

        });
        log.info("end san permissions: {}", System.currentTimeMillis() - start);
        log.info("list new permissions: {}", new ObjectMapper().writeValueAsString(permissions));
        if (!permissions.isEmpty()) {
            permissionService.saveAll(permissions);
        }
    }

    private boolean isRequestMappingAnnotation(Annotation an) {
        return an instanceof GetMapping
            || an instanceof PostMapping
            || an instanceof PutMapping
            || an instanceof DeleteMapping
            || an instanceof PatchMapping
            || an instanceof RequestMapping;
    }

    private void processMapping(Annotation annotation, Permission permission, String classEndpoint) {
        var endpoint = "";
        var methodType = annotation.annotationType().getSimpleName();
        try {
            Method valueMethod = annotation.annotationType().getMethod("value");
            String[] values = (String[]) valueMethod.invoke(annotation);
            endpoint = values.length > 0 ? values[0] : "";
            Method methodMethod = annotation.annotationType().getMethod("method");
            RequestMethod[] methods = (RequestMethod[]) methodMethod.invoke(annotation);
            methodType = methods.length > 0 ? methods[0].toString() : "";

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        methodType = methodType.toUpperCase().replace("MAPPING", "");
        permission.setEndpoint(classEndpoint + endpoint);
        permission.setMethodType(methodType);
    }
}
