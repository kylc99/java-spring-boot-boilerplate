package com.mewebstudio.javaspringbootboilerplate;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

@Tag("context")
@SpringBootTest
@DisplayName("Context test for MainApplication")
class MainApplicationTest {

    @Autowired
    private ApplicationContext applicationContext;

    @Test
    @DisplayName("Context loads successfully")
    void applicationContextLoadedTest() {
        // Assert that the application context is not null
        assertNotNull(applicationContext, "Application context should be loaded");

    }
}
